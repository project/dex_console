<?php

declare(strict_types=1);

namespace Drupal\dex_console;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\Compiler\DexCompilerPass as CoreDexCompilerPass;

/**
 * Service provider.
 */
final class DexConsoleServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    // Prevent doubling up of functionality/clashing bin files.
    if (\class_exists(CoreDexCompilerPass::class)) {
      throw new \LogicException('Do not use this module in combination with the core patch. Uninstall this module first.');
    }

    $container
      ->addCompilerPass(new DexCompilerPass(), priority: 0);
  }

}
